
import model.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        List<Student> students = new ArrayList<Student>();

        students.add(new Student(1,"Artemiy","Baranets","Gennadievich",2000,"Pririch","0677032879","Kiber",4, "TK - 4"));
        students.add(new Student(2,"Brook","Greys","Axes",2001,"Doto","1488322228","DotaClub",6, "GNP"));
        students.add(new Student(1,"Alex","Dols","Olegov",1999,"Geroev","0656789987","Dut",4, "Dypa - 4"));
        students.add(new Student(1,"Vasia","Popkin","Lohov",2000,"Donbas","14881588","Kiber",4, "TK - 4"));

    }

    static List<Student> studentsInFaculty(List<Student> students, String faculty) {
        return students.stream()
                .filter((a) -> a.getFaculty().equals(faculty))
                .collect(Collectors.toList());
    }

    static List<Student> studentsInFacultyAndCourse(List<Student> students, String faculty, int course){
        return students.stream()
                .filter((a) -> a.getFaculty().equals(faculty) && a.getCourse() == course)
                .collect(Collectors.toList());
    }

    static List<Student> studentsInYearOfBirth(List<Student> students, int year){
        return students.stream()
                .filter((a) -> a.getYearOfBirth() >= year)
                .collect(Collectors.toList());
    }

    static List<String> studentsInGroup(List<Student> students, String group){

        List<String> list =students.stream()
                .filter((a) -> a.getGroup().equals(group))
                .map(a -> a.getFirstName() + ", "
                        + a.getLastName()+", "
                        + a.getPatronymic())
                .collect(Collectors.toList());

        return list;
    }

    static List<String> outputStudent(List<Student> students){

        List<String> list =students.stream()
                .map(student -> student.getFirstName() +", "
                        + student.getLastName() + ", "
                        + student.getPatronymic() + " - "
                        + student.getFaculty() +", "
                        + student.getGroup())
                .collect(Collectors.toList());

        for(String str: list){
            System.out.println(str);
        }

        return list;
    }

    static long countStudentInCourse(List<Student> students,int course){
        return students.stream()
                .filter(a -> course == a.getCourse())
                .count();
    }

}
