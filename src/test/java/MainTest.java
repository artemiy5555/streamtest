import model.Student;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;


public class MainTest {

    List<Student> students = new ArrayList<Student>();

    @Before
    public void setUp() throws Exception {

        students.add(new Student(1,"Artemiy","Baranets","Gennadievich",2000,"Pririch","0677032879","Kiber",4, "TK - 4"));
        students.add(new Student(2,"Brook","Greys","Axes",2001,"Doto","1488322228","DotaClub",6, "GNP"));
        students.add(new Student(1,"Alex","Dols","Olegov",1999,"Geroev","0656789987","Dut",4, "Dypa - 4"));
        students.add(new Student(1,"Vasia","Popkin","Lohov",2000,"Donbas","14881588","Kiber",4, "TK - 4"));

    }

    @Test
    public void studentsInFaculty_Test() {

        List<Student> expected = new ArrayList<Student>();
        expected.add(new Student(1,"Artemiy","Baranets","Gennadievich",2000,"Pririch","0677032879","Kiber",4, "TK - 4"));
        expected.add(new Student(1,"Vasia","Popkin","Lohov",2000,"Donbas","14881588","Kiber",4, "TK - 4"));

        List<Student> actual = Main.studentsInFaculty(students,"Kiber");

        assertEquals(expected.size(),actual.size());
        //assertArrayEquals(expected.toArray(), actual.toArray());

    }

    @Test
    public void studentsInFacultyAndCourse() {

        List<Student> expected = new ArrayList<Student>();
        expected.add(new Student(1,"Artemiy","Baranets","Gennadievich",2000,"Pririch","0677032879","Kiber",4, "TK - 4"));
        expected.add(new Student(1,"Vasia","Popkin","Lohov",2000,"Donbas","14881588","Kiber",4, "TK - 4"));

        List<Student> actual = Main.studentsInFacultyAndCourse(students,"Kiber",4);

        assertEquals(expected.size(), actual.size());
        //assertArrayEquals(expected.toArray(), actual.toArray());

    }

    @Test
    public void studentsInYearOfBirth() {

        List<Student> expected = new ArrayList<Student>();
        expected.add(new Student(1,"Artemiy","Baranets","Gennadievich",2000,"Pririch","0677032879","Kiber",4, "TK - 4"));
        expected.add(new Student(2,"Brook","Greys","Axes",2001,"Doto","1488322228","DotaClub",6, "GNP"));
        expected.add(new Student(1,"Vasia","Popkin","Lohov",2000,"Donbas","14881588","Kiber",4, "TK - 4"));
        List<Student> actual = Main.studentsInYearOfBirth(students,2000);

        assertEquals(expected.size(), actual.size());
        //assertArrayEquals(expected.toArray(), actual.toArray());

    }

    @Test
    public void studentsInGroup() {

        List<String> expected = new ArrayList<>();
        expected.add("Artemiy, Baranets, Gennadievich");
        expected.add("Vasia, Popkin, Lohov");

        List<String> actual = Main.studentsInGroup(students,"TK - 4");

        assertEquals(expected, actual);
        //assertArrayEquals(expected.toArray(), actual.toArray());

    }

    @Test
    public void outputStudent() {

        List<String> expected = new ArrayList<>();
        expected.add("Artemiy, Baranets, Gennadievich - Kiber, TK - 4");
        expected.add("Brook, Greys, Axes - DotaClub, GNP");
        expected.add("Alex, Dols, Olegov - Dut, Dypa - 4");
        expected.add("Vasia, Popkin, Lohov - Kiber, TK - 4");

        List<String> actual = Main.outputStudent(students);

        assertEquals(expected, actual);
    }

    @Test
    public void countStudentInCourse() {

        long expected = 3;

        long actual = Main.countStudentInCourse(students,4);

        assertEquals(expected,actual);
    }

}